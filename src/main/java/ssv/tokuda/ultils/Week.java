/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.ultils;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Dcmt
 */

public class Week {
  public String plus(String startWeek, Integer numOfWeek) {
    String weekOfYear = "";
    int year = Integer.parseInt(startWeek.substring(0, 4));
    int week = Integer.parseInt(startWeek.substring(4, 6));
    Calendar calendar = new GregorianCalendar();
    calendar.set(Calendar.YEAR, year);
    calendar.set(Calendar.WEEK_OF_YEAR, week + numOfWeek - 1);
    calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
    calendar.setFirstDayOfWeek(Calendar.SUNDAY);
    if (calendar.get(Calendar.WEEK_OF_YEAR) < 10) {
      weekOfYear = "0" + calendar.get(Calendar.WEEK_OF_YEAR);
    } else {
      weekOfYear = String.valueOf(calendar.get(Calendar.WEEK_OF_YEAR));
    }
    return calendar.get(Calendar.YEAR) + weekOfYear;
  }
}
