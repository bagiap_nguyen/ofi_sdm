/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ssv.tokuda.model.Project;
import ssv.tokuda.model.repository.ProjectRepository;

/**
 * Dcmt
 */

@RestController
@RequestMapping("/hello")
public class HelloController {
  
  @Autowired
  private ProjectRepository projectRepository;
  
  @RequestMapping("/demo")
  public void demo() {
    Optional<Project> pro = projectRepository.findById(new Long(2));
    if(pro.isPresent()) {
      System.out.println(pro.get().getDescription());
    }
  }
}
