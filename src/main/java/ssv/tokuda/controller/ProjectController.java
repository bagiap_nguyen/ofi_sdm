package ssv.tokuda.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ssv.tokuda.controller.dto.ProjectCreateBasicDTO;
import ssv.tokuda.controller.dto.ProjectCreateDTO;
import ssv.tokuda.controller.dto.ProjectCreateTacticalDTO;
import ssv.tokuda.controller.dto.ProjectListDTO;
import ssv.tokuda.model.Project;
import ssv.tokuda.model.service.ProjectService;

@RestController
@RequestMapping("/project")
public class ProjectController {
	
	@Autowired
	private ProjectService projectService;
	
	@RequestMapping("/list")
	public List<ProjectListDTO> list() {
		List<Project> list = projectService.list();
		return ProjectListDTO.toList(list);
	}
	
	@RequestMapping("/create/basic")
	public ProjectCreateDTO createBasic(@RequestParam("json") String json,
      @RequestParam("files") Optional<MultipartFile> files) {
	  ProjectCreateBasicDTO dto = ProjectCreateBasicDTO.toDTO(json);
	  dto.setFiles(files);
	  Project project = dto.toModel();
	  return projectService.createBasic(project,dto);
	}
	
	@RequestMapping("/create/tactical")
	public void createTactical(@RequestParam("json") String json) {
	  ProjectCreateTacticalDTO dto = ProjectCreateTacticalDTO.toDTO(json);
	  System.out.println(dto);
	}
}
