/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.controller.dto;

import lombok.Data;
import ssv.tokuda.model.Project;

/**
 * Dcmt
 */

@Data
public class ProjectCreateDTO {
  private ProjectCreateBasicDTO basic;
  private ProjectCreateTacticalDTO tacticals;
  private ProjectCreateHypothesisDTO hypothesis;

  public ProjectCreateDTO(Project project, ProjectCreateBasicDTO basicDTO,
      ProjectCreateHypothesisDTO hypothesisDTO) {
    setBasic(basicDTO);
    setHypothesis(hypothesisDTO);
   
  }
}
