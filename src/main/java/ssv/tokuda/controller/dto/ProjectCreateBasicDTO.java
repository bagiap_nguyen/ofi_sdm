/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.controller.dto;

import java.io.IOException;
import java.util.Optional;
import org.springframework.beans.BeanUtils;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Data;
import ssv.tokuda.model.Project;

/**
 * Dcmt
 */
@Data
public class ProjectCreateBasicDTO {
  private Long id = 0L;
  private String name = "";
  private String startYear = "";
  private String startWeekNumber = "";
  private Integer numberOfWeek = 0;
  private String description = "";
  private Optional<MultipartFile> files = Optional.empty();

  private static final ObjectMapper MAPPER = new ObjectMapper();

  public static ProjectCreateBasicDTO toDTO(String json) {
    try {
      MAPPER.registerModule(new JavaTimeModule());
      return MAPPER.readValue(json, ProjectCreateBasicDTO.class);
    } catch (IOException e) {
      e.printStackTrace();
      throw new IllegalStateException(e.getMessage());
    }
  }

  public Project toModel() {
    Project model = new Project();
    BeanUtils.copyProperties(this, model);
    return model;
  }

  public String startWeek() {
    return getStartYear() + getStartWeekNumber();
  }
}
