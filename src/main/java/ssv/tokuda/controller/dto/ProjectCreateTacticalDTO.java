/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.controller.dto;

import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Data;

/**
 * Dcmt
 */
@Data
public class ProjectCreateTacticalDTO {
  private Long id = 0L;
  private String tacticalProduct;
  private String tacticalPromotion;
  private String tacticalTarget;

  private static final ObjectMapper MAPPER = new ObjectMapper();

  public static ProjectCreateTacticalDTO toDTO(String json) {
    try {
      MAPPER.registerModule(new JavaTimeModule());
      return MAPPER.readValue(json, ProjectCreateTacticalDTO.class);
    } catch (IOException e) {
      e.printStackTrace();
      throw new IllegalStateException(e.getMessage());
    }
  }
}
