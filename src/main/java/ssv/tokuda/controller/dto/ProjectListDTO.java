/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.controller.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;
import ssv.tokuda.model.Project;

/**
 * Dcmt
 */
@Data
@NoArgsConstructor
public class ProjectListDTO {
	private Long id;
	private String name = "";
	private String sku = "";
	private String targetMonth;
	private String startWeek;
	private Integer numberOfWeek = 0;
	private String tacticalProduct = "";
	private String tacticalPromotion = "";
	private String tacticalTarget = "";
	private BigDecimal projectExpenses;
	private String importStatus = "";
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	private LocalDateTime createdDatetime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	private LocalDateTime updateDatetime;
	private Boolean isDeleted = false;
	
	public static List<ProjectListDTO> toList(List<Project> list) {
		return list.stream().map(p -> {			
			return new ProjectListDTO(p);
		}).collect(Collectors.toList());
	}
	
	public ProjectListDTO(Project project) {
		BeanUtils.copyProperties(project,this);	
	}
	
	
}
