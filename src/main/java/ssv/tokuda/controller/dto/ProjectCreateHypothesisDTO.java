/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.controller.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;
import lombok.Data;

/**
 * Dcmt
 */

@Data
public class ProjectCreateHypothesisDTO {

  private Long id;
  private Long projectId;
  private String imageUrl = "";
  private String description = "";
  private BigDecimal ancientPrice;
  private BigDecimal numberOfSales;
  private BigDecimal salesNumberPerWeek = new BigDecimal(0);
  private BigDecimal amount = new BigDecimal(0);
  private BigDecimal standardCost = new BigDecimal(0);
  private Optional<List<MultipartFile>> files = Optional.empty();
  
  private List<ProjectCreateSalesDTO> salesList ;
  private BigDecimal projectExpenses = new BigDecimal(0);
  
}
