/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.controller.dto;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import lombok.Data;
import ssv.tokuda.model.ProjectSalesPlan;

/**
 * Dcmt
 */

@Data
public class ProjectCreateSalesDTO {
  private Long id;
  private Long projectId;
  private String week = "";
  private Float compositionRatio;
  private Boolean isPeak;

  public static List<ProjectCreateSalesDTO> toList(List<ProjectSalesPlan> list) {
    return list.stream().map(p -> {
      return new ProjectCreateSalesDTO(p);
    }).collect(Collectors.toList());
  }

  public ProjectCreateSalesDTO(ProjectSalesPlan sales) {
    BeanUtils.copyProperties(sales, this);
  }
}
