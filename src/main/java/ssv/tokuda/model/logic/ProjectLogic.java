/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.model.logic;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.val;
import ssv.tokuda.controller.dto.ProjectCreateBasicDTO;
import ssv.tokuda.controller.dto.ProjectCreateDTO;
import ssv.tokuda.controller.dto.ProjectCreateHypothesisDTO;
import ssv.tokuda.controller.dto.ProjectCreateSalesDTO;
import ssv.tokuda.model.Project;
import ssv.tokuda.model.ProjectSalesPlan;
import ssv.tokuda.model.repository.ProjectRepository;
import ssv.tokuda.model.repository.ProjectSalesPlanRepository;
import ssv.tokuda.model.service.ProjectService;
import ssv.tokuda.ultils.Week;

/**
 * Dcmt
 */

@Service
public class ProjectLogic implements ProjectService {

  @Autowired
  private ProjectRepository projectRepository;

  @Autowired
  private ProjectSalesPlanRepository projectSalesPlanRepository;

  @Override
  public List<Project> list() {
    return projectRepository.findAll();
  }

  @Override
  public ProjectCreateDTO createBasic(Project project, ProjectCreateBasicDTO dto) {

    project.setStartWeek(dto.startWeek());
    project = projectRepository.save(project); 
    val response = new ProjectCreateDTO(project,dto,createBasicHypothesis(project,dto));
    return response;

  }

  private List<ProjectSalesPlan> createBasicSalesPlan(Project project, ProjectCreateBasicDTO dto) {
    projectSalesPlanRepository.deleteByProjectId(project.getId());
    dto.setId(project.getId());
    IntStream.range(0, dto.getNumberOfWeek()).forEach(i -> {
      String weekOfYear = new Week().plus(dto.startWeek(), i + 1);
      Optional<ProjectSalesPlan> model =
          projectSalesPlanRepository.findBy(project.getId(), weekOfYear);
      if (model.isPresent()) {
        ProjectSalesPlan entity = model.get();
        entity.setWeek(weekOfYear);
        projectSalesPlanRepository.save(entity);
      } else {
        ProjectSalesPlan target = new ProjectSalesPlan(project);
        target.setWeek(weekOfYear);
        projectSalesPlanRepository.save(target);
      }
    });
    return projectSalesPlanRepository.findByProjectId(project.getId());
  }
  
  private ProjectCreateHypothesisDTO createBasicHypothesis(Project project, ProjectCreateBasicDTO dto) {
    ProjectCreateHypothesisDTO hypothesis = new ProjectCreateHypothesisDTO();
    hypothesis.setSalesList(ProjectCreateSalesDTO.toList(createBasicSalesPlan(project,dto)));    
    return hypothesis;
  }

}
