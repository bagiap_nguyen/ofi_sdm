/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.model.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ssv.tokuda.model.Project;

/**
 * Dcmt
 */
public interface ProjectRepository extends JpaRepository<Project, Long> {
  
  @Query(value = "SELECT * FROM project WHERE is_deleted = 0 ", nativeQuery = true)
  List<Project> findAll();
  
  @Query(value = "SELECT * FROM Project WHERE is_deleted = 0 and id = ?1 ", nativeQuery = true)
  Optional<Project> findById(Long id);
}
