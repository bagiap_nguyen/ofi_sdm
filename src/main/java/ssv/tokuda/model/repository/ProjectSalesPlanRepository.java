/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.model.repository;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ssv.tokuda.model.ProjectSalesPlan;

/**
 * Dcmt
 */

public interface ProjectSalesPlanRepository extends JpaRepository<ProjectSalesPlan, Long>{
  
  @Query(value = "SELECT * FROM project_sales_plan WHERE is_deleted = 0 and project_id = ?1 and week = ?2 ", nativeQuery = true)
  Optional<ProjectSalesPlan> findBy(Long projectId,String week);
  
  @Modifying
  @Transactional
  @Query(value="DELETE FROM project_sales_plan WHERE project_id = ?1 ",nativeQuery = true)
  void deleteByProjectId(Long projectId);
  
  @Query(value = "SELECT * FROM project_sales_plan WHERE is_deleted = 0 and project_id = ?1", nativeQuery = true)
  List<ProjectSalesPlan> findByProjectId(Long projectId);
}
