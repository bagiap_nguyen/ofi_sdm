/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * Dcmt
 */


@Data
@Entity
@Table(name = "url")
public class Url {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private Long id;
  private Long projectEnforcementId;
  private String url = "";
  
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  private LocalDateTime createdDatetime = LocalDateTime.now();

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  private LocalDateTime updateDatetime = LocalDateTime.now();

  private Boolean isDeleted = false;
}
