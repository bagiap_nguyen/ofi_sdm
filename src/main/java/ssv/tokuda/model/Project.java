/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;


/**
 * Dcmt.
 */


@Data
@Entity
@Table(name = "project")
public class Project {
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private Long id;
  private String name;
  private String sku;
  private String targetMonth;
  private String startWeek;
  private Integer numberOfWeek;
  private String description;
  private String tacticalProduct;
  private String tacticalPromotion;
  private String tacticalTarget;
  private BigDecimal projectExpenses;
  private String importStatus;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  private LocalDateTime createdDatetime = LocalDateTime.now();

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  private LocalDateTime updateDatetime = LocalDateTime.now();

  private Boolean isDeleted = false;
  
  public List<ProjectSalesPlan> salesPlan() { 
    return null;
  } 
  
}
