/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * Dcmt.
 */


@Data
@Entity
@Table(name = "project_goods")
public class ProjectGoods {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private Long id;

  private Long projectId;
  private String imageUrl = "";
  private String description = "";
  private BigDecimal ancientPrice;
  private BigDecimal actualAncientPrice = new BigDecimal(0);
  private BigDecimal numberOfSales;
  private BigDecimal actualNumberOfSales;
  private BigDecimal salesNumberPerWeek = new BigDecimal(0);
  private BigDecimal actualSalesNumberPerWeek = new BigDecimal(0);
  private BigDecimal amount = new BigDecimal(0);
  private BigDecimal actualAmount = new BigDecimal(0);
  private BigDecimal standardCost = new BigDecimal(0);
  private BigDecimal actualTotalStandardCost = new BigDecimal(0);

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  private LocalDateTime createdDatetime = LocalDateTime.now();

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  private LocalDateTime updateDatetime = LocalDateTime.now();

  private Boolean isDeleted = false;

}
