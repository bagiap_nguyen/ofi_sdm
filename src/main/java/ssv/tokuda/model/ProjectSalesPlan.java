/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * Dcmt
 */

@Data
@Entity
@Table(name = "project_sales_plan")
public class ProjectSalesPlan {
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private Long id;
  
  private Long projectId;
  private String week = "";
  private Float compositionRatio ;
  private BigDecimal actualAmount = new BigDecimal(0);
  private BigDecimal actual_sales_number = new BigDecimal(0);
  private BigDecimal actual_sales_profit = new BigDecimal(0);
  private Boolean isPeak = false;
  
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  private LocalDateTime createdDatetime = LocalDateTime.now();

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  private LocalDateTime updateDatetime = LocalDateTime.now();

  private Boolean isDeleted = false;
  
  public ProjectSalesPlan(Project project){
    setProjectId(project.getId());
  }
  
  
}
