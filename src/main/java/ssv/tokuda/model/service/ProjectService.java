/**
 * TokudaFanpageClub.
 */
package ssv.tokuda.model.service;

import java.util.List;
import ssv.tokuda.controller.dto.ProjectCreateBasicDTO;
import ssv.tokuda.controller.dto.ProjectCreateDTO;
import ssv.tokuda.model.Project;

/**
 * Dcmt
 */

public interface ProjectService {
	List<Project> list();
	ProjectCreateDTO createBasic(Project project,ProjectCreateBasicDTO dto);
}
